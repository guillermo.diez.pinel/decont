#Download all the files specified in data/filenames

# La opción -i nos permite descargar todos los ficheros cuyos enlaces de descarga se encuentren en data/urls
# La opción -nc evita descargarlos si ya existen
wget -nc -i data/urls -P data



# Download the contaminants fasta file, and uncompress it
if [ ! -f "res/contaminants.fasta.gz" ]
then
    bash scripts/download.sh https://bioinformatics.cnio.es/data/courses/decont/contaminants.fasta.gz res yes #TODO
else
    echo "El fichero contaminants.fasta.gz ya existe"
fi



# Index the contaminants file
if [ -z "$(ls -A res/contaminants_idx)" ]
then
    bash scripts/index.sh res/contaminants.fasta res/contaminants_idx
else
    echo "El genoma ya ha sido indexado, pasando al siguiente paso"
fi



# Merge the samples into a single file
mkdir -p out/merged

touch data/sid_list.txt
for file in data/*fastq.gz
do
    filename=$(basename ${file})
    sid=$(echo $filename | cut -d "-" -f 1)
    
    if grep "$sid" data/sid_list.txt
    then
    echo "$sid ya existe en la lista"
    elif echo $filename | grep "RNA"
    then
    echo $sid >> data/sid_list.txt
    echo "$sid se añadió exitosamente a la lista"
    fi
done


for sid in $(cat data/sid_list.txt)
do
    if [ -f "out/merged/${sid}_merged.fastq.gz" ]
    then
        echo "El fichero merge para la muestra $sid ya existe"
    else
        bash scripts/merge_fastqs.sh data out/merged $sid
    fi
done



# TODO: run cutadapt for all merged files
mkdir -p out/cutadapt
mkdir -p log/cutadapt
for file in out/merged/*
do
    file=$(basename ${file})
    file=$(echo $file | cut -d "." -f 1)
    
    if [ -f "out/cutadapt/${file}_trimmed.fastq.gz" ]
    then
        echo "El fichero trimmed para ${file} ya existe"
    else
    echo "################ Running cutadapt on ${file} ##################"
    cutadapt -m 18 -a TGGAATTCTCGGGTGCCAAGG --discard-untrimmed -o out/cutadapt/${file}_trimmed.fastq.gz out/merged/${file}.fastq.gz > log/cutadapt/${file}.log
    fi
done

#TODO: run STAR for all trimmed files
for file in out/cutadapt/*.fastq.gz
do
    # you will need to obtain the sample ID from the filename
    sid=$(basename ${file})
    sid=$(echo $sid | awk -F'_merged' '{print $1}')
    mkdir -p out/star/$sid

    if [ -z "$(ls -A out/star/${sid}/)" ]
    then
        echo "################# Running STAR on sample $sid #################"
        STAR --runThreadN 4 --genomeDir res/contaminants_idx --outReadsUnmapped Fastx --readFilesIn ${file} --readFilesCommand gunzip -c --outFileNamePrefix out/star/${sid}/
    else
        echo "STAR ya fue ejecutado para la muestra $sid"
    fi
done 

# TODO: create a log file containing information from cutadapt and star logs
# (this should be a single log file, and information should be *appended* to it on each run)
# - cutadapt: Reads with adapters and total basepairs
if [ -s Log.out ]
then
    echo "Log.out ya contiene información, no se modificará"
else
    for file in log/cutadapt/*
    do
        sid=$(basename ${file})
        sid=$(echo $sid | awk -F'_merged' '{print $1}')
        printf "\nCutadapt log summary for $sid\n" >> Log.out
        grep "Reads with adapters" ${file} >> Log.out
        grep "Total basepairs processed" ${file} >> Log.out
    done
# - star: Percentages of uniquely mapped reads, reads mapped to multiple loci, and to too many loci
# tip: use grep to filter the lines you're interested in
    for sid in $(cat data/sid_list.txt)
    do
        printf "\n STAR log summary for $sid\n" >> Log.out
        grep "Uniquely mapped reads %" out/star/${sid}/Log.final.out >> Log.out
        grep "% of reads mapped to multiple loci" out/star/${sid}/Log.final.out >> Log.out
        grep "% of reads mapped to too many loci" out/star/${sid}/Log.final.out >> Log.out
    done
fi
