#Download all the files specified in data/filenames
touch data/sid_list.txt

for url in $(cat data/urls) #TODO
do
    bash scripts/download.sh $url data
done

# Download the contaminants fasta file, and uncompress it
bash scripts/download.sh https://bioinformatics.cnio.es/data/courses/decont/contaminants.fasta.gz res yes #TODO

# Merge the samples into a single file
mkdir -p out/merged
for sid in $(cat data/sid_list.txt) #TODO
do
    bash scripts/merge_fastqs.sh data out/merged $sid
done
