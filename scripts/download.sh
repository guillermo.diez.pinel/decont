# This script should download the file specified in the first argument ($1), place it in the directory specified in the second argument ($2), 
# and *optionally* uncompress the downloaded file with gunzip if the third argument ($3) contains the word "yes".

wget -P $2 $1
filename=$(basename "$1")
sid=$(echo $filename | cut -d "-" -f 1)

#if grep "$sid" data/sid_list.txt
#then
#echo "Sample ID $sid already added"
#elif echo $filename | grep "RNA"
#then
#echo $sid >> data/sid_list.txt
#echo "Sample ID was successfully added"
#fi

if [ "$3" = "yes" ]
then
    gunzip -k "res/$filename"
fi

