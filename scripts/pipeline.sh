#Download all the files specified in data/filenames

#for url in $(cat data/urls)
#do
#    bash scripts/download.sh $url data
#done

wget -i data/urls -P data

# Download the contaminants fasta file, and uncompress it
bash scripts/download.sh https://bioinformatics.cnio.es/data/courses/decont/contaminants.fasta.gz res yes #TODO

# Index the contaminants file
bash scripts/index.sh res/contaminants.fasta res/contaminants_idx

# Merge the samples into a single file
mkdir -p out/merged

touch data/sid_list.txt
for file in data/*fastq.gz
do
    filename=$(basename ${file})
    sid=$(echo $filename | cut -d "-" -f 1)
    
    if grep "$sid" data/sid_list.txt
    then
    echo "Sample ID $sid already added"
    elif echo $filename | grep "RNA"
    then
    echo $sid >> data/sid_list.txt
    echo "Sample ID was successfully added"
    fi
done

for sid in $(cat data/sid_list.txt) #TODO
do
    bash scripts/merge_fastqs.sh data out/merged $sid
done
# TODO: run cutadapt for all merged files
mkdir -p out/cutadapt
mkdir -p log/cutadapt
for file in out/merged/*
do
    file=$(basename ${file})
    file=$(echo $file | cut -d "." -f 1)
    echo "################ Running cutadapt on ${file} ##################"
    cutadapt -m 18 -a TGGAATTCTCGGGTGCCAAGG --discard-untrimmed -o out/cutadapt/${file}_trimmed.fastq.gz out/merged/${file}.fastq.gz > log/cutadapt/${file}.log
    cat log/cutadapt/${file}.log
done

#TODO: run STAR for all trimmed files
for file in out/cutadapt/*.fastq.gz
do
    # you will need to obtain the sample ID from the filename
    sid=$(basename ${file})
    sid=$(echo $sid | awk -F'_merged' '{print $1}')
    mkdir -p out/star/$sid
    echo "################# Running STAR on sample $sid #################"
    STAR --runThreadN 4 --genomeDir res/contaminants_idx --outReadsUnmapped Fastx --readFilesIn ${file} --readFilesCommand gunzip -c --outFileNamePrefix out/star/${sid}/
done 

# TODO: create a log file containing information from cutadapt and star logs
# (this should be a single log file, and information should be *appended* to it on each run)
# - cutadapt: Reads with adapters and total basepairs
for file in log/cutadapt/*
do
    sid=$(basename ${file})
    sid=$(echo $sid | awk -F'_merged' '{print $1}')
    printf "\nCutadapt log summary for $sid\n" >> Log.out
    grep "Reads with adapters" ${file} >> Log.out
    grep "Total basepairs processed" ${file} >> Log.out
done
# - star: Percentages of uniquely mapped reads, reads mapped to multiple loci, and to too many loci
# tip: use grep to filter the lines you're interested in
for sid in $(cat data/sid_list.txt)
do
    printf "\n STAR log summary for $sid\n" >> Log.out
    grep "Uniquely mapped reads %" out/star/${sid}/Log.final.out >> Log.out
    grep "% of reads mapped to multiple loci" out/star/${sid}/Log.final.out >> Log.out
    grep "% of reads mapped to too many loci" out/star/${sid}/Log.final.out >> Log.out
done
