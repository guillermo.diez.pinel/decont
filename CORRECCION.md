### pipeline.sh

- [+0.25] Reemplazo de loop con wget one-liner

- [+0.25] Chequeo de pre-existencia de archivos

- [-1] La lista de sids puede resolverse en el encabezado del `for` con un `ls` y pipes (`|`)

- [-0.5] El chequeo de la existencia de los archivos mejor hacerla en download.sh, donde se puede armar el nombre del fichero y se evita de esa manera escribirlo en el código principal.
